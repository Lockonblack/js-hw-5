/////--------------Теоретичні питання-------
//1 Метод об'єкта це функція яка належить об'єкту.


//2 Властивість є значення або набір значень (у вигляді масиву або об'єкта),
// який є членом об'єкта і може містити будь який тип даних.

//3 Читання властивості, наприклад, крапкою . в obj.method() повертає не саме
// значення властивості, але спеціальне значення “посилального типу”, яке зберігає
// як значення властивості, так і об’єкт, з якою він був взятий.
// Це використовується для подальшого виклику методу за допомогою (), щоб отримати об’єкт
// і встановити this до цього.
// Для всіх інших операцій, посилальний тип автоматично стає значенням властивості
// (функцією у нашому випадку).



function createNewUser() {
    let firstName = prompt("Введіть своє ім'я:")
    let lastName = prompt("Введіть прізвище:")

const newUser = {
    firstName_: firstName,
    lastName_: lastName,
    getLogin: function () {
        return(this.firstName_[0] + this.lastName_).toLowerCase();
    },
    // setFirstName: function (newFirstName){
    //     if (typeof newFirstName === 'string'){
    //         this.firstName_ = newFirstName;
    //     }
    //         else {
    //             console.log("Невалідне значення імені")
    //     }
    // },
    // setLastName: function (newLastName) {
    //     if (typeof newLastName === 'string') {
    //         this.lastName_ = newLastName;
    //     } else {
    //         console.log("Невалідне значення для прізвища.")
    //     }
    // }
};
Object.defineProperty(newUser, "firstName", {writable: false});
Object.defineProperty( newUser, "lastName", {writable: false});
return newUser;
}

 let user = createNewUser();
console.log(user.getLogin());

// user.setFirstName("Kolya");
// user.setLastName("Levine");
// console.log(user.getLogin());